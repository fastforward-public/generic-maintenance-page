package main

import "github.com/go-logr/logr"

type logWriter struct {
	log logr.Logger
}

func (w logWriter) Write(p []byte) (n int, err error) {
	w.log.Info(string(p))
	return len(p), nil
}
