DOCKER_IMAGE=registry.gitlab.com/fastforward-websolutions/k8s/generic-maintenance-page

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -h -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the go server.
	go build -o error-server

docker-build: ## Build docker image of static error site.
	docker build -t $(DOCKER_IMAGE) .

docker-run: ## Start server locally.
	docker run -it --rm -p 8080:8080 -e FORWARD_CODE=true $(DOCKER_IMAGE)
