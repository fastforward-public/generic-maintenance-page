package main

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"regexp"
	"strconv"
	"time"

	"github.com/go-logr/logr"
	"github.com/gorilla/handlers"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"k8s.io/klog/v2/klogr"
)

const (
	// FormatHeader name of the header used to extract the format
	FormatHeader = "X-Format"

	// CodeHeader name of the header used as source of the HTTP status code to
	// return
	CodeHeader = "X-Code"

	// ContentType name of the header that defines the format of the reply
	ContentType = "Content-Type"

	// OriginalURI name of the header with the original URL from NGINX
	OriginalURI = "X-Original-URI"

	// Namespace name of the header that contains information about the Ingress
	// namespace
	Namespace = "X-Namespace"

	// IngressName name of the header that contains the matched Ingress
	IngressName = "X-Ingress-Name"

	// ServiceName name of the header that contains the matched Service in the
	// Ingress
	ServiceName = "X-Service-Name"

	// ServicePort name of the header that contains the matched Service port in
	// the Ingress
	ServicePort = "X-Service-Port"

	// RequestId is a unique ID that identifies the request - same as for
	// backend service
	RequestId = "X-Request-ID"

	// EnvFilesPath is the name of the environment variable indicating the
	// location on disk of files served by the handler.
	EnvFilesPath      = "ERROR_FILES_PATH"
	EnvDefaultAddress = "LISTEN_ADDRESS"
	// This is a prefix for setting upstream header values. Text after this will
	// be the header's name and the value the value of the header.
	EnvHeadersPrefix = "HEADERS_"
	EnvForwardCode   = "FORWARD_CODE" // Forward status codes to upstream. If false, always send 200 OK.

	DefaultIndexFile = "index.html"
)

var (
	log    logr.Logger
	assets *regexp.Regexp
)

func init() {
	prometheus.MustRegister(requestCount)
	prometheus.MustRegister(requestDuration)
	log = klogr.New()
	assets = regexp.MustCompile(`.(png|jpg|jpeg|gif|css|js|woff2|eot|ttf|woff|ico)$`)
}

func main() {
	errFilesPath := "/www"
	if os.Getenv(EnvFilesPath) != "" {
		errFilesPath = os.Getenv(EnvFilesPath)
	}

	address := ":8080"
	if env := os.Getenv(EnvDefaultAddress); env != "" {
		address = env
	}

	hdl := http.NewServeMux()
	hdl.HandleFunc("/", errorHandler(errFilesPath, DefaultIndexFile))
	hdl.Handle("/metrics", promhttp.Handler())
	hdl.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	log.Info("server listening on", "address", address)
	loggingHandler := handlers.CombinedLoggingHandler(logWriter{log}, hdl)
	http.ListenAndServe(address, loggingHandler)
}

func errorHandler(dir, defaultIndex string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		defer func() {
			duration := time.Since(start).Seconds()
			proto := strconv.Itoa(r.ProtoMajor)
			proto = fmt.Sprintf("%s.%s", proto, strconv.Itoa(r.ProtoMinor))
			requestCount.WithLabelValues(proto).Inc()
			requestDuration.WithLabelValues(proto).Observe(duration)
		}()

		if os.Getenv("DEBUG") != "" {
			w.Header().Set(FormatHeader, r.Header.Get(FormatHeader))
			w.Header().Set(CodeHeader, r.Header.Get(CodeHeader))
			w.Header().Set(ContentType, r.Header.Get(ContentType))
			w.Header().Set(OriginalURI, r.Header.Get(OriginalURI))
			w.Header().Set(Namespace, r.Header.Get(Namespace))
			w.Header().Set(IngressName, r.Header.Get(IngressName))
			w.Header().Set(ServiceName, r.Header.Get(ServiceName))
			w.Header().Set(ServicePort, r.Header.Get(ServicePort))
			w.Header().Set(RequestId, r.Header.Get(RequestId))
		}

		code := http.StatusOK
		codeHdr := r.Header.Get(CodeHeader)
		if codeHdr != "" {
			w.Header().Set(CodeHeader, codeHdr)
			var err error
			code, err = strconv.Atoi(codeHdr)
			if err != nil {
				log.Error(err, "cannot convert value from header to number", "value", codeHdr)
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}
			if code < 200 || code > 599 {
				log.Info("invalid code from upstream", "code", code)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}
			// Exclude assets from sending 503 code.
			if assets.MatchString(path.Base(r.URL.Path)) || os.Getenv(EnvForwardCode) == "" {
				code = http.StatusOK
			}
			w = &responseWriter{
				ResponseWriter: w,
				override: map[int]int{
					http.StatusOK: code,
				},
			}
		} else {
			w = &responseWriter{ResponseWriter: w}
		}

		headers := additionalHeaders()
		for k, v := range headers {
			w.Header().Set(k, v)
		}

		hdl := http.FileServer(rewritingFileSystem{
			DefaultIndexFile: defaultIndex,
			FileSystem:       http.Dir(dir),
		})
		hdl.ServeHTTP(w, r)
	}
}
