package main

import (
	"errors"
	"io/fs"
	"net/http"
	"os"
	"strings"
	"sync"
)

func additionalHeaders() map[string]string {
	out := make(map[string]string)
	vars := os.Environ()
	for _, v := range vars {
		env := strings.Split(v, "=")
		if !strings.HasPrefix(env[0], EnvHeadersPrefix) {
			continue
		}
		key := strings.TrimPrefix(env[0], EnvHeadersPrefix)
		key = strings.ReplaceAll(key, "_", "-")
		out[key] = env[1]
	}
	return out
}

// responseWriter is more lenient towards calling WriteHeader multiple times. It
// will just do nothing if the request has already been sent off.
type responseWriter struct {
	http.ResponseWriter
	sync.Once

	override map[int]int // Map status codes set in WriteHeader to other status codes.
}

// WriteHeader is a no-op on this responseWriter if it has been called before.
func (w *responseWriter) WriteHeader(statusCode int) {
	w.Do(func() {
		if w.override != nil {
			if code, ok := w.override[statusCode]; ok {
				statusCode = code
			}
		}
		w.ResponseWriter.WriteHeader(statusCode)
	})
}

type rewritingFileSystem struct {
	DefaultIndexFile string // Path to the file we send when 404.
	http.FileSystem
}

func (fsys rewritingFileSystem) Open(name string) (http.File, error) {
	file, err := fsys.FileSystem.Open(name)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			// Try a dir further "up" and see if we can get the requested file.
			i := strings.Index(name, "/")
			if i < 0 {
				// We already are on the root path. If it's probably not an
				// asset, send default index file.
				if !assets.MatchString(name) {
					return fsys.FileSystem.Open(fsys.DefaultIndexFile)
				}
				return nil, err // 404.
			}
			name = name[i+1:]
			return fsys.Open(name) // Recurse.
		}
		return nil, err
	}
	return file, err
}
