function getFirstMatch(array, filterCallback) {
    const filteredArray = array.filter(filterCallback);
    return !!filteredArray && filteredArray.length
        ? filteredArray[0]
        : undefined;
}

function getBrowserLocales(options = {}) {
    const defaultOptions = {
        languageCodeOnly: true,
        fallback: 'de',
        allowedLanguages: undefined
    };

    const opt = {
        ...defaultOptions,
        ...options,
    };
    console.log(opt);

    const browserLocales =
        navigator.languages === undefined // https://developer.mozilla.org/en-US/docs/Web/API/Navigator/languages
            ? [navigator.language] // IE fix https://developer.mozilla.org/en-US/docs/Web/API/Navigator/language
            : navigator.languages;

    let userLanguages;
    if (!browserLocales) {
        userLanguages = [opt.fallback];
    } else {
        userLanguages = browserLocales.map(locale => {
            const trimmedLocale = locale.trim();

            return opt.languageCodeOnly
                ? trimmedLocale.split(/-|_/)[0]
                : trimmedLocale;
        });
    }

    return !!opt.allowedLanguages ?
        getFirstMatch(userLanguages, language => opt.allowedLanguages.indexOf(language) !== -1)
        : userLanguages;
}

function propertyByString(object, propertyPath) {
    propertyPath = propertyPath.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    propertyPath = propertyPath.replace(/^\./, '');           // strip a leading dot
    var a = propertyPath.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in object) {
            object = object[k];
        } else {
            return;
        }
    }
    return object;
}

function initTranslator() {
    const language = getBrowserLocales({allowedLanguages: ['de', 'fr', 'it']})
    const translateableElements = document.querySelectorAll('[t]');
    const languageTranslations = window[language];
    translateableElements.forEach(element => {
        const translationKey = element.getAttribute('t');
        const translation = propertyByString(languageTranslations, translationKey);
        if (!!translation) {
            element.innerHTML = translation;
        }

    })
}

