var de = {
    "title": "Wartung",
    "error": {
        "title": "Wir sind bald wieder zurück!",
        "description": "Entschuldigung für die Unannehmlichkeiten. Wir führen im Moment Wartungsarbeiten durch."
    },
};
var fr = {
    "title": "Maintenance",
    "error": {
        "title": "Nous reviendrons bientôt!",
        "description": "Désolé pour le dérangement. Nous effectuons des travaux de maintenance en ce moment."
    },
};
var it = {
    "title": "Maintenance",
    "error": {
        "title": "We’ll be back soon!",
        "description": "Sorry for the inconvenience. We're performing some maintenance at the moment."
    },
};
