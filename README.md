# Generic Maintenance Page

The Source for the static unavailable page for generic usage.

It will serve all content from `src/` with the `src/index.html` page as the
index. Requests for non-existing files will be answered as if the index pages
had been requested.

## Special Setup

This will try to deploy the error page image and patch the relevant ingress to
redirect/intercept errors and send them to this error-page image.

The image supports metrics at `/metrics` and a healthcheck on `/healthz`.
